package graphics;

import music.*;
import parser.ParsingException;
import parser.VirtualPianoParser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;


public class PianoKey extends JComponent {
    public enum LabelTextChoice {
        NO_TEXT, KEY_CODE, NOTE_NAME
    }

    private static final int DEFAULT_INSTRUMENT = 1;
    private static final int VELOCITY = 50;
    private static boolean isMousePressed;
    private static MidiChannel channel;

    private int midiNumber;
    private boolean isPlaying;
    private boolean isKeyDown;

    private int keyCode;
    private boolean isShift;

    private String keyCodeRepresentation;
    private String noteString;

    private JLabel label = new JLabel();

    private Paintable paintComponentFunction = null;

    private List<PianoEventListener> pianoEventListenerList = new ArrayList<>();

    public void addPianoEventListener(PianoEventListener listener) {
        this.pianoEventListenerList.add(listener);
    }

    static {
        try {
            Synthesizer synthesizer = MidiSystem.getSynthesizer();
            synthesizer.open();
            PianoKey.channel =  synthesizer.getChannels()[DEFAULT_INSTRUMENT];

        } catch (MidiUnavailableException e) {
            e.printStackTrace();
        }

    }

    {
        this.setPreferredSize(new Dimension(26, 140));
        this.setLayout(new OverlayLayout(this));
        this.addPianoKeyListeners();

        this.label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        this.label.setAlignmentY(JLabel.BOTTOM_ALIGNMENT);
        this.add(this.label);
    }

    PianoKey() {
        super();
        // needed in InvisiblePianoKey
    }

    private PianoKey(int midiNumber) {
        this.midiNumber = midiNumber;
    }

    private Pitch pitch;
    private boolean isSharp;
    private Octave octave;

    public PianoKey(VirtualPianoParser parser, Pitch pitch, boolean isShift, Octave octave) {
        this.pitch = pitch;
        this.isSharp = isShift;
        this.octave = octave;

        this.noteString = pitch.toString();
        if (isShift) {
            this.noteString += "#";
        }

        this.noteString += octave.getValue();

        this.midiNumber = parser.getMidiNumber(noteString);
        this.keyCodeRepresentation = Character.toString(parser.getNoteChar(noteString));
        this.keyCode = this.getKeyCode();
        this.isShift = isShift;
    }

    private int getKeyCode() {
        if (Character.isLetter(this.keyCodeRepresentation.charAt(0))) {
            return KeyEvent.VK_A + this.keyCodeRepresentation.toLowerCase().charAt(0) - 'a';
        }
        else if (Character.isDigit(this.keyCodeRepresentation.charAt(0))) {
            return KeyEvent.VK_1 + this.keyCodeRepresentation.toLowerCase().charAt(0) - '1';
        }
        else {
            String digitUpper = ")!@#$%^&*(";
            int digitUpperIndex = digitUpper.indexOf(this.keyCodeRepresentation.charAt(0));
            if (digitUpperIndex == -1) {
                return KeyEvent.VK_M;   // Exception
            }

            return KeyEvent.VK_0 + digitUpperIndex;
        }

    }

    private long startTimeMillis;

    private void startPlaying() {
        if (this.isPlaying) {
            return;
        }

        this.isPlaying = true;
        this.repaint();
        PianoKey.channel.noteOn(this.midiNumber, PianoKey.VELOCITY);
        // TODO: implement
        this.startTimeMillis = System.currentTimeMillis();
    }

    private void stopPlaying() {
        if (!this.isPlaying) {
            return;
        }

        this.isPlaying = false;
        this.repaint();
        PianoKey.channel.noteOff(this.midiNumber, PianoKey.VELOCITY);
        // TODO: implement
        long currentTimeMillis = System.currentTimeMillis();
        Duration duration;
        if (currentTimeMillis - this.startTimeMillis > Duration.oneQuarter.toMilis()) {
            duration = Duration.oneQuarter;
        }
        else {
            duration = Duration.oneEight;
        }

        this.pianoEventListenerList.forEach(listener -> listener.dispatchEvent(new Note(duration, this.pitch, this.isSharp, this.octave)));

    }

    void setPaintComponentFunction(Paintable paintComponentFunction) {
        this.paintComponentFunction = paintComponentFunction;
    }

    boolean isPlaying() {
        return this.isPlaying;
    }

    void setLabelText(LabelTextChoice labelTextChoice) {
        this.label.setForeground(Color.cyan);
        switch (labelTextChoice) {
            case NO_TEXT:
                this.label.setText("");
                break;
            case KEY_CODE:
                this.label.setText(this.keyCodeRepresentation);
                break;
            case NOTE_NAME:
                this.label.setText(this.noteString);
                break;
        }

    }

    public void processKeyEvent(KeyEvent e) {
        super.processKeyEvent(e);
        // needed in Keyboard
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (this.paintComponentFunction != null) {
            this.paintComponentFunction.paint(g);
            return;
        }

        // Default painting
        if (this.isPlaying) {
            g.setColor(Color.blue);
        }
        else {
            g.setColor(Color.cyan);
        }

        g.fill3DRect(0, 0, this.getWidth() - 1, this.getHeight() - 1, true);
    }

    private void addPianoKeyListeners() {

        // Keyboard playing
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == PianoKey.this.keyCode) {
                    PianoKey.this.isKeyDown = true;
                    if ((e.getModifiersEx() & KeyEvent.SHIFT_DOWN_MASK) == KeyEvent.SHIFT_DOWN_MASK
                            == PianoKey.this.isShift) {
                        PianoKey.this.startPlaying();
                    }

                }

                if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
                    if (PianoKey.this.isShift && PianoKey.this.isKeyDown) {
                        PianoKey.this.startPlaying();
                    }
                    else {
                        PianoKey.this.stopPlaying();
                    }

                }

            }

            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                if (e.getKeyCode() == PianoKey.this.keyCode) {
                    PianoKey.this.isKeyDown = false;
                    PianoKey.this.stopPlaying();
                }

                if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
                    if (PianoKey.this.isShift) {
                        PianoKey.this.stopPlaying();
                    }
                    else if (PianoKey.this.isKeyDown) {
                        PianoKey.this.startPlaying();
                    }

                }

            }

        });

        // Mouse playing
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                PianoKey.isMousePressed = true;
                PianoKey.this.startPlaying();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                PianoKey.isMousePressed = false;
                PianoKey.this.stopPlaying();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                if (PianoKey.isMousePressed) {
                    PianoKey.this.startPlaying();
                }

            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                if (PianoKey.isMousePressed) {
                    PianoKey.this.stopPlaying();
                }
            }

        });

    }

    public static void main(String ... args) {
        JFrame frame = new JFrame("PianoKey test");

        frame.setBounds(40, 40, 51, 140);

        JPanel panel = new JPanel(new GridLayout(1, 2));
        PianoKey key;

        key = new PianoKey(67);
        key.setPaintComponentFunction(g -> {
            if (key.isPlaying()) {
                g.setColor(new Color(226, 81, 255));
            }
            else {
                g.setColor(new Color(0x46FF0A));
            }

            g.fillRect(0, 0, key.getWidth() - 1, key.getHeight() - 1);
            g.setColor(new Color(0xFF5000));
            g.drawRect(0, 0, key.getWidth() - 1, key.getHeight() - 1);
        });

        panel.add(key);
        frame.add(panel);

        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                key.processKeyEvent(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                key.processKeyEvent(e);
            }

        });

        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        try {
            VirtualPianoParser vpp = new VirtualPianoParser();
            vpp.parseConfigFile("C:\\Users\\Bogdan\\Downloads\\map.csv");
            PianoPlayer player = new PianoPlayer();
            player.add(vpp.parseComposition("C:\\Users\\Bogdan\\Dcuments\\GitHub\\VirtualPianoConverter\\VirtualPianoConverter\\resource\\input\\fur_elise.txt"));
            Thread.sleep(1000);
            player.start();
        }
        catch (ParsingException e) {
            JOptionPane.showMessageDialog(frame, e.getMessage());
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
