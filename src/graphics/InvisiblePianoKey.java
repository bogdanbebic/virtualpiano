package graphics;

public class InvisiblePianoKey extends PianoKey {
    public InvisiblePianoKey() {
        super();
        this.setVisible(false);
    }

}
