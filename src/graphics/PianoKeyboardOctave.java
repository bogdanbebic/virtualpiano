package graphics;

import music.Octave;
import music.Pitch;
import parser.VirtualPianoParser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.stream.Stream;

public class PianoKeyboardOctave extends JLayeredPane {
    private static final int NUMBER_OF_WHITE_KEYS = 7, NUMBER_OF_BLACK_KEYS = 5;
    private PianoKey [] whiteKeys;
    private PianoKey [] blackKeys;
    private Octave octave;

    private Paintable paintComponentFunction = null;

    /**
     * @param paintComponentFunction Function which paints this component
     *                               (should not be used except for custom border painting)
     */
    public void setPaintComponentFunction(Paintable paintComponentFunction) {
        this.paintComponentFunction = paintComponentFunction;
    }

    Stream<PianoKey> getAllPianoKeys() {
        return Stream.concat(Arrays.stream(this.whiteKeys), Arrays.stream(this.blackKeys));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (this.paintComponentFunction != null) {
            this.paintComponentFunction.paint(g);
        }

    }

    public PianoKeyboardOctave(VirtualPianoParser parser, Octave octave) {
        this.octave = octave;
        this.constructKeys(parser);
        this.layOutComponents();
        this.delegateKeyEvents();
    }


    // constructs the keys contained within one octave
    private void constructKeys(VirtualPianoParser parser) {
        this.whiteKeys = new PianoKey[PianoKeyboardOctave.NUMBER_OF_WHITE_KEYS];
        this.blackKeys = new PianoKey[PianoKeyboardOctave.NUMBER_OF_BLACK_KEYS + 3];

        // construct white keys
        for (int i = 0; i < this.whiteKeys.length; i++) {
            this.whiteKeys[i] = new PianoKey(parser, Pitch.values()[i], false, this.octave);
            this.whiteKeys[i].setBorder(BorderFactory.createLineBorder(Color.green));
            PianoKey key = this.whiteKeys[i];
            key.setPaintComponentFunction(g -> {
                if (key.isPlaying()) {
                    g.setColor(new Color(0xC8C8C8));
                } else {
                    g.setColor(new Color(0xFFFFFF));
                }

                g.fillRect(0, 0, key.getWidth() - 1, key.getHeight() - 1);
            });

        }

        // construct black keys
        int j = 0;
        for (int i = 0; i < this.blackKeys.length; i++) {
            if (i == 0 || i == 3 || i == this.blackKeys.length - 1) {
                this.blackKeys[i] = new InvisiblePianoKey();
                if (i == 3) {
                    j++;
                }

                continue;
            }

            this.blackKeys[i] = new PianoKey(parser, Pitch.values()[j++], true, this.octave);
            this.blackKeys[i].setBorder(BorderFactory.createLineBorder(Color.gray));
            PianoKey key = this.blackKeys[i];
            this.blackKeys[i].setPaintComponentFunction(g -> {
                if (key.isPlaying()) {
                    g.setColor(new Color(0x373737));
                }
                else {
                    g.setColor(new Color(0x0));
                }

                g.fillRect(0, 0, key.getWidth() - 1, key.getHeight() - 1);
            });
        }
    }

    /// Lays out components graphically
    private void layOutComponents() {
        this.setLayout(new OverlayLayout(this));
        JPanel whiteKeysPanel
                = new JPanel(new GridLayout(1, PianoKeyboardOctave.NUMBER_OF_WHITE_KEYS));

        // maybe implement better with GridBagLayout
        JPanel blackKeysPanel
                = new JPanel(new GridLayout(2,
                PianoKeyboardOctave.NUMBER_OF_BLACK_KEYS + 3,
                24, 10));

        Arrays.stream(this.whiteKeys).forEach(whiteKeysPanel::add);
        Arrays.stream(this.blackKeys).forEach(blackKeysPanel::add);
        for (int i = 0; i < PianoKeyboardOctave.NUMBER_OF_BLACK_KEYS + 3; i++) {
            blackKeysPanel.add(new InvisiblePianoKey());
        }

        blackKeysPanel.setOpaque(false);

        this.add(whiteKeysPanel, new Integer(0));
        this.add(blackKeysPanel, new Integer(1));
    }

    public void processKeyEvent(KeyEvent e) {
        super.processKeyEvent(e);
        // needed in Keyboard
    }

    void setLabelText(PianoKey.LabelTextChoice labelTextChoice) {
        Arrays.stream(this.blackKeys).forEach(blackKey -> blackKey.setLabelText(labelTextChoice));
        Arrays.stream(this.whiteKeys).forEach(whiteKey -> whiteKey.setLabelText(labelTextChoice));
    }

    /// Sends key events to all keys
    private void delegateKeyEvents() {
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                Stream.concat(Arrays.stream(PianoKeyboardOctave.this.blackKeys),
                        Arrays.stream(PianoKeyboardOctave.this.whiteKeys))
                        .forEach(key -> key.processKeyEvent(e));

            }

            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                Stream.concat(Arrays.stream(PianoKeyboardOctave.this.blackKeys),
                        Arrays.stream(PianoKeyboardOctave.this.whiteKeys))
                        .forEach(key -> key.processKeyEvent(e));

            }

        });

    }

    public static void main(String ... args) {
        //Create and set up the window.
        JFrame frame = new JFrame("KeyboardOctave test");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        JComponent newContentPane = new PianoKeyboardOctave(new VirtualPianoParser(), Octave.THREE);
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

}
