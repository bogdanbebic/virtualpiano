package graphics;

import java.awt.Graphics;

@FunctionalInterface
public interface Paintable {
    void paint(Graphics g);
}
