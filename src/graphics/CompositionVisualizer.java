package graphics;

import music.*;
import parser.VirtualPianoParser;

import javax.swing.*;
import java.awt.*;

public class CompositionVisualizer extends JComponent implements PianoEventListener {

    private static final int QUARTER_NOTE_PIXEL_OFFSET = 64;
    private static final int EIGHT_NOTE_PIXEL_OFFSET = QUARTER_NOTE_PIXEL_OFFSET / 2;

    public static final int MUSIC_SYMBOL_HEIGHT = 32;

    private Composition composition;

    private boolean showNoteBindings = false;
    private VirtualPianoParser parser;

    CompositionVisualizer(VirtualPianoParser parser) {
        this.parser = parser;
    }

    void setShowNoteBindings(boolean showNoteBindings) {
        this.showNoteBindings = showNoteBindings;
        this.repaint();
    }

    void setComposition(Composition composition) {
        this.composition = composition;
        this.currentIndex = 0;
        this.repaint();
    }

    private long lastEventTimeMillis = 0;
    private boolean running = true;

    public void destroy() {
        this.running = false;
    }

    private Thread pause14CheckThread = new Thread(() -> {
        while (CompositionVisualizer.this.running) {
            long startSleepTimeMillis = System.currentTimeMillis();
            try {
                Thread.sleep(Duration.oneQuarter.toMilis());

            } catch (InterruptedException ignored) {}

            if (startSleepTimeMillis > CompositionVisualizer.this.lastEventTimeMillis) {
                CompositionVisualizer.this.dispatchPause(Duration.oneQuarter);
            }

        }

    });

    private Thread pause18CheckThread = new Thread(() -> {
        while (CompositionVisualizer.this.running) {
            long startSleepTimeMillis = System.currentTimeMillis();
            try {
                Thread.sleep(Duration.oneEight.toMilis());

            } catch (InterruptedException ignored) {}

            if (startSleepTimeMillis > CompositionVisualizer.this.lastEventTimeMillis) {
                CompositionVisualizer.this.dispatchPause(Duration.oneEight);
            }

        }

    });

    {
        this.pause14CheckThread.start();
        this.pause18CheckThread.start();
    }

    private void dispatchPause(Duration duration) {
        if (this.composition != null && this.composition.get(this.currentIndex) instanceof Pause) {
            if (duration == ((Pause) this.composition.get(this.currentIndex)).getDuration()) {
                this.currentIndex++;
                this.repaint();
            }

        }

    }

    private Chord chord = new Chord(Duration.oneQuarter);

    private Thread chordThread = new Thread(() -> {
        while (CompositionVisualizer.this.running) {
            synchronized (CompositionVisualizer.this) {
                if (CompositionVisualizer.this.composition != null
                    && CompositionVisualizer.this.composition.get(this.currentIndex) instanceof Chord) {

                    CompositionVisualizer.this.chord = new Chord(Duration.oneQuarter);
                }

            }

            try {
                Thread.sleep(Duration.oneQuarter.toMilis());
            }
            catch (InterruptedException ignored) {}

            synchronized (CompositionVisualizer.this) {
                if (CompositionVisualizer.this.composition != null
                        && CompositionVisualizer.this.composition.get(this.currentIndex) instanceof Chord) {

                    CompositionVisualizer.this.dispatchChord(CompositionVisualizer.this.chord);
                }

            }

        }

    });

    {
        this.chordThread.start();
    }

    private void dispatchChord(Chord chord) {
        if (this.composition != null && this.composition.get(this.currentIndex) instanceof Chord) {
            if (this.composition.get(this.currentIndex).equals(chord)) {
                this.currentIndex++;
                this.repaint();
            }

        }

    }

    @Override
    public void dispatchEvent(Note note) {
        this.lastEventTimeMillis = System.currentTimeMillis();

        if (this.composition == null || note == null) {
            return;
        }

        if (note.equals(this.composition.get(this.currentIndex))) {
            this.currentIndex++;
        }
        else {
            if (note.getDuration() == Duration.oneQuarter) {
                synchronized (this) {
                    this.chord.add(note);
                }

            }

        }

        if (this.currentIndex == this.composition.size()) {
            this.currentIndex = 0;
        }

        this.repaint();
    }

    private int currentIndex = 0;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        // draw background
        g.setColor(Color.gray);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        // draw grid
        g.setColor(Color.darkGray);
        g.drawRect(0, 0, this.getWidth(), this.getHeight());
        for (int i = 0; i < this.getWidth();
             i += CompositionVisualizer.QUARTER_NOTE_PIXEL_OFFSET) {

            g.drawLine(i, 0, i, this.getHeight());
        }

        if (this.composition == null) {
            return;
        }

        int currentX = 0;
        for (int i = currentIndex; i < this.composition.size(); i++) {
            if (this.composition.get(i) instanceof MusicSymbol) {
                MusicSymbol currentMusicSymbol = (MusicSymbol) this.composition.get(i);
                int width = 0;
                if (currentMusicSymbol.getDuration() == Duration.oneEight) {
                    width = CompositionVisualizer.EIGHT_NOTE_PIXEL_OFFSET;
                }
                else if (currentMusicSymbol.getDuration() == Duration.oneQuarter) {
                    width = CompositionVisualizer.QUARTER_NOTE_PIXEL_OFFSET;
                }

                currentMusicSymbol.draw(g, currentX, 0, width, this.showNoteBindings, this.parser);
                currentX += width;
            }

        }

    }



}
