package graphics;

import export.ExportException;
import export.MidiFormatter;
import export.TxtFormatter;
import music.Composition;
import music.Octave;
import music.PianoPlayer;
import music.PianoRecorder;
import parser.VirtualPianoParser;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class VirtualPiano extends JFrame {
    private class UserActionsPanel extends JPanel {

        // change configuration
        // private JButton loadConfigurationButton = new JButton("Load configuration");

        // composition load and play buttons
        private JButton openCompositionButton   = new JButton("Open composition");
        private JButton playCompositionButton   = new JButton("Play composition");

        // recording buttons
        private JButton startRecordingButton    = new JButton("Record");
        private JButton stopRecordingButton     = new JButton("Stop recording");

        // exporting buttons
        private JButton midiExportButton    = new JButton("Midi export");
        private JButton txtExportButton     = new JButton("Txt export");

        // radio buttons for piano key markings
        private JRadioButton noKeyAssist    = new JRadioButton("No key assist", true);
        private JRadioButton keyAssist      = new JRadioButton("Display key", false);
        private JRadioButton noteAssist     = new JRadioButton("Display note", false);

        // file chooser for composition
        private JFileChooser compositionFileChooser = new JFileChooser(".");

        private JFileChooser exportMidiFileChooser  = new JFileChooser(".");

        private JFileChooser exportTxtFileChooser   = new JFileChooser(".");

        // file choosers initialization
        {
            this.compositionFileChooser.setFileFilter(
                    new FileNameExtensionFilter("Composition txt files", "txt")
            );

            this.exportMidiFileChooser.setFileFilter(
                    new FileNameExtensionFilter("Composition midi files", "midi")
            );

            this.exportTxtFileChooser.setFileFilter(
                    new FileNameExtensionFilter("Composition txt files", "txt")
            );
        }

        // initialize radio buttons
        {
            ButtonGroup radioButtonGroup = new ButtonGroup();

            this.noKeyAssist.setMnemonic(KeyEvent.VK_1);
            this.noKeyAssist.setToolTipText("(alt+1) "
                    + "Disables text displayed on piano keys");
            this.noKeyAssist.setBackground(Color.gray);
            this.noKeyAssist.setBorderPainted(true);
            this.noKeyAssist.setBorder(BorderFactory.createLineBorder(Color.darkGray));
            radioButtonGroup.add(this.noKeyAssist);

            this.keyAssist.setMnemonic(KeyEvent.VK_2);
            this.keyAssist.setToolTipText("(alt+2) "
                    + "Enables text displayed on piano keys - displays key");
            this.keyAssist.setBackground(Color.gray);
            this.keyAssist.setBorderPainted(true);
            this.keyAssist.setBorder(BorderFactory.createLineBorder(Color.darkGray));
            radioButtonGroup.add(this.keyAssist);

            this.noteAssist.setMnemonic(KeyEvent.VK_3);
            this.noteAssist.setToolTipText("(alt+3) "
                    + "Enables text displayed on piano keys - displays note");
            this.noteAssist.setBackground(Color.gray);
            this.noteAssist.setBorderPainted(true);
            this.noteAssist.setBorder(BorderFactory.createLineBorder(Color.darkGray));
            radioButtonGroup.add(this.noteAssist);

            this.noKeyAssist.addActionListener(l -> {
                VirtualPiano.this.keyboard.setLabelText(PianoKey.LabelTextChoice.NO_TEXT);
                VirtualPiano.this.compositionVisualizer.setShowNoteBindings(false);
                VirtualPiano.this.requestFocus();
            });

            this.keyAssist.addActionListener(l -> {
                VirtualPiano.this.keyboard.setLabelText(PianoKey.LabelTextChoice.KEY_CODE);
                VirtualPiano.this.compositionVisualizer.setShowNoteBindings(true);
                VirtualPiano.this.requestFocus();
            });

            this.noteAssist.addActionListener(l -> {
                VirtualPiano.this.keyboard.setLabelText(PianoKey.LabelTextChoice.NOTE_NAME);
                VirtualPiano.this.compositionVisualizer.setShowNoteBindings(false);
                VirtualPiano.this.requestFocus();
            });
        }

        // initialize load and play and add action listeners
        {
            this.openCompositionButton.setMnemonic(KeyEvent.VK_O);
            this.openCompositionButton.setToolTipText(
                    "(alt+O) Loads a composition from a file");
            this.openCompositionButton.addActionListener(l -> {
                if (compositionFileChooser.showOpenDialog(VirtualPiano.this) == JFileChooser.APPROVE_OPTION) {
                    try {
                        VirtualPiano.this.loadedComposition = VirtualPiano.this.parser.parseComposition(
                                this.compositionFileChooser.getSelectedFile().getAbsolutePath()
                        );

                        VirtualPiano.this.compositionVisualizer.setComposition(VirtualPiano.this.loadedComposition);
                        UserActionsPanel.this.playCompositionButton.setEnabled(true);
                        UserActionsPanel.this.midiExportButton.setEnabled(true);
                        UserActionsPanel.this.txtExportButton.setEnabled(true);
                    }
                    catch (Exception e) {
                        JOptionPane.showMessageDialog(
                                VirtualPiano.this,
                                e.getMessage(),
                                "Error",
                                JOptionPane.ERROR_MESSAGE
                        );

                    }

                }

                VirtualPiano.this.requestFocus();
            });

            this.playCompositionButton.setEnabled(false);
            this.playCompositionButton.setMnemonic(KeyEvent.VK_P);
            this.playCompositionButton.setToolTipText(
                    "(alt+P) Plays the currently loaded composition");
            this.playCompositionButton.addActionListener(l -> {
                VirtualPiano.this.requestFocus();
                VirtualPiano.this.playLoadedComposition();
            });
        }


        // initialize recording and add action listeners
        {
            this.startRecordingButton.setMnemonic(KeyEvent.VK_R);
            this.startRecordingButton.setToolTipText("(alt+R) Starts recording of the played music");
            this.startRecordingButton.addActionListener(l -> {
                VirtualPiano.this.recorder.startRecording();
                UserActionsPanel.this.startRecordingButton.setEnabled(false);
                UserActionsPanel.this.stopRecordingButton.setEnabled(true);
                VirtualPiano.this.requestFocus();
            });

            this.stopRecordingButton.setEnabled(false);
            this.stopRecordingButton.setMnemonic(KeyEvent.VK_R);
            this.stopRecordingButton.setToolTipText("(alt+R) Stops recording of the played music");
            this.stopRecordingButton.addActionListener(l -> {
                VirtualPiano.this.recordedComposition = VirtualPiano.this.recorder.getRecordedComposition();
                UserActionsPanel.this.stopRecordingButton.setEnabled(false);
                UserActionsPanel.this.startRecordingButton.setEnabled(true);
                UserActionsPanel.this.playCompositionButton.setEnabled(true);
                UserActionsPanel.this.midiExportButton.setEnabled(true);
                UserActionsPanel.this.txtExportButton.setEnabled(true);
                VirtualPiano.this.requestFocus();
            });
        }

        // initialize export and add action listeners
        {
            String exportToolTipText = "Exports the currently recorded composition (if it exists)";

            this.midiExportButton.setEnabled(false);
            this.midiExportButton.setMnemonic(KeyEvent.VK_M);
            this.midiExportButton.setToolTipText("(alt+M) " + exportToolTipText
                    + ", exports loaded composition otherwise");
            this.midiExportButton.addActionListener(l -> {
                if (UserActionsPanel.this.exportMidiFileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                    VirtualPiano.this.exportMidi(
                            UserActionsPanel.this.exportMidiFileChooser.getSelectedFile().getAbsolutePath()
                    );

                }

                VirtualPiano.this.requestFocus();
            });

            this.txtExportButton.setEnabled(false);
            this.txtExportButton.setMnemonic(KeyEvent.VK_T);
            this.txtExportButton.setToolTipText("(alt+T) " + exportToolTipText);
            this.txtExportButton.addActionListener(l -> {
                if (UserActionsPanel.this.exportTxtFileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                    VirtualPiano.this.exportTxt(
                            UserActionsPanel.this.exportTxtFileChooser.getSelectedFile().getAbsolutePath()
                    );

                }

                VirtualPiano.this.requestFocus();
            });
        }

        // sets the layout and adds components
        {
            this.setBackground(Color.gray);
            this.setLayout(new GridLayout(3, 3));
            this.add(openCompositionButton);
            this.add(startRecordingButton);
            this.add(midiExportButton);
            this.add(playCompositionButton);
            this.add(stopRecordingButton);
            this.add(txtExportButton);
            this.add(this.noKeyAssist);
            this.add(this.keyAssist);
            this.add(this.noteAssist);
        }

    }

    private static final int HEIGHT = 400, WIDTH = 1600;

    private VirtualPianoParser parser   = new VirtualPianoParser();
    private PianoKeyboard keyboard      = new PianoKeyboard(parser, Octave.TWO, Octave.THREE, Octave.FOUR, Octave.FIVE, Octave.SIX);

    private TxtFormatter txtFormatter   = new TxtFormatter(this.parser);
    private MidiFormatter midiFormatter = new MidiFormatter(this.parser);

    private CompositionVisualizer compositionVisualizer = new CompositionVisualizer(this.parser);
    private UserActionsPanel userActionsPanel           = new UserActionsPanel();

    private Composition loadedComposition   = null;
    private Composition recordedComposition = null;

    private PianoRecorder recorder = new PianoRecorder();

    {
        this.keyboard.getAllPianoKeys().forEach(pianoKey -> pianoKey.addPianoEventListener(this.recorder));
        this.keyboard.getAllPianoKeys().forEach(pianoKey -> pianoKey.addPianoEventListener(this.compositionVisualizer));
    }

    private void playLoadedComposition() {
        PianoPlayer player = new PianoPlayer();
        player.setParser(this.parser);
        Composition composition = this.loadedComposition;
        if (this.recordedComposition != null) {
            composition = this.recordedComposition;
        }

        player.add(composition);
        player.start();
    }

    private void exportMidi(String outPath) {
        try {
            if (this.recordedComposition != null) {
                this.midiFormatter.export(outPath, this.recordedComposition);
            } else {
                this.midiFormatter.export(outPath, this.loadedComposition);
            }

        }
        catch (ExportException e) {
            JOptionPane.showMessageDialog(
                    this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );

        }
    }

    private void exportTxt(String outPath) {
        try {
            if (this.recordedComposition != null) {
                this.txtFormatter.export(outPath, this.recordedComposition);
            } else {
                this.txtFormatter.export(outPath, this.loadedComposition);
            }

        }
        catch (ExportException e) {
            JOptionPane.showMessageDialog(
                    this,
                    e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );

        }

    }

    public VirtualPiano() {
        super("Virtual piano");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.arrangeGraphicalComponents();

        this.setSize(VirtualPiano.WIDTH, VirtualPiano.HEIGHT);
        this.setVisible(true);
    }

    private void arrangeGraphicalComponents() {
        JPanel rootPanel = new JPanel(new GridLayout(2, 1));
        JPanel controlPanel = new JPanel(new GridLayout(1, 2));

        controlPanel.add(this.userActionsPanel);
        controlPanel.add(this.compositionVisualizer);

        rootPanel.add(controlPanel);
        rootPanel.add(this.keyboard);

        this.add(rootPanel);
    }

    {
        this.delegateKeyEvents();
    }

    private void delegateKeyEvents() {
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (!e.isAltDown()) {
                    VirtualPiano.this.keyboard.processKeyEvent(e);
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                if (!e.isAltDown()) {
                    VirtualPiano.this.keyboard.processKeyEvent(e);
                }

            }

        });

        this.setFocusable(true);
        this.requestFocus();
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        new VirtualPiano();
    }

    public static void main(String ... args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(VirtualPiano::createAndShowGUI);
    }

}
