package graphics;

import music.*;
import parser.VirtualPianoParser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.stream.Stream;

public class PianoKeyboard extends JComponent {

    private Paintable paintComponentFunction = null;

    /**
     * @param paintComponentFunction Function which paints this component
     *                               (should not be used except for custom border painting)
     */
    public void setPaintComponentFunction(Paintable paintComponentFunction) {
        this.paintComponentFunction = paintComponentFunction;
    }

    private PianoKeyboardOctave [] keyboardOctaves;

    {
        this.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        this.delegateKeyEvents();
    }

    public PianoKeyboard(VirtualPianoParser parser, Octave ... octaves) {
        this.setLayout(new GridLayout(1, octaves.length));
        this.keyboardOctaves = new PianoKeyboardOctave[octaves.length];

        for (int i = 0; i < octaves.length; i++) {
            this.keyboardOctaves[i] = new PianoKeyboardOctave(parser, octaves[i]);
            this.keyboardOctaves[i].setBorder(BorderFactory.createLineBorder(Color.red));
            this.add(this.keyboardOctaves[i]);
        }

    }

    Stream<PianoKey> getAllPianoKeys() {
        Stream<PianoKey> ret = this.keyboardOctaves[0].getAllPianoKeys();
        for (int i = 1; i < this.keyboardOctaves.length; i++) {
            ret = Stream.concat(ret, this.keyboardOctaves[i].getAllPianoKeys());
        }

        return ret;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (paintComponentFunction != null) {
            paintComponentFunction.paint(g);
        }

    }

    public void processKeyEvent(KeyEvent e) {
        super.processKeyEvent(e);
        // needed in Keyboard
    }

    public void setLabelText(PianoKey.LabelTextChoice labelTextChoice) {
        Arrays.stream(this.keyboardOctaves)
                .forEach(keyboardOctave -> keyboardOctave.setLabelText(labelTextChoice));

    }

    private void delegateKeyEvents() {
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                Arrays.stream(PianoKeyboard.this.keyboardOctaves)
                        .forEach(key -> key.processKeyEvent(e));

            }

            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                Arrays.stream(PianoKeyboard.this.keyboardOctaves)
                        .forEach(key -> key.processKeyEvent(e));

            }

        });

    }

    public static void main(String ... args) {
        JFrame frame = new JFrame("Keyboard test");
        frame.setSize(1000, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 5));

        for (Octave octave : Octave.values()) {
            PianoKeyboardOctave keyboardOctave = new PianoKeyboardOctave(new VirtualPianoParser(), octave);
            keyboardOctave.setBorder(BorderFactory.createLineBorder(Color.red));
            panel.add(keyboardOctave);
        }

        frame.add(panel);
        frame.setVisible(true);
    }

    /*
    public static void main1(String ... args) {

        List<PianoKey> list = new ArrayList<>();

        int[] keyCodes = {
                KeyEvent.VK_D,
                KeyEvent.VK_D,
                KeyEvent.VK_F,
                KeyEvent.VK_F,
                KeyEvent.VK_G,
                KeyEvent.VK_G,
                KeyEvent.VK_H,
                KeyEvent.VK_H,
                KeyEvent.VK_J,
                KeyEvent.VK_J,
                KeyEvent.VK_K,
                KeyEvent.VK_K,
                KeyEvent.VK_L,
                KeyEvent.VK_L,
                KeyEvent.VK_SEMICOLON
        };

        boolean isShift = true;
        for (int i = 0; i < keyCodes.length; i++) {
            PianoKey key = new PianoKey(52 + 2 * i);
            key.setKeyCode(keyCodes[i], isShift = !isShift);
            key.setLabelText(isShift ? PianoKey.LabelTextChoice.KEY_CODE : PianoKey.LabelTextChoice.KEY_CODE);
            list.add(key);
        }

        JPanel panel = new JPanel(new GridLayout(1, list.size()));
        list.forEach(panel::add);

        Dimension d = list.get(0).getPreferredSize();

        JFrame frame = new JFrame("PianoKeyboard test");
        frame.setBounds(40, 40, d.width * list.size(), d.height);

        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                for (PianoKey key : list) {
                    key.processKeyEvent(e);
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                for (PianoKey key : list) {
                    key.processKeyEvent(e);
                }

            }

        });

        frame.add(panel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        PianoPlayer player = new PianoPlayer();
        for (int i = 0; i < 7; i++) {
            player.add(new Note(Duration.oneQuarter, Pitch.C, Octave.THREE));
            player.add(new Pause(Duration.oneEight));
        }

        player.start();

    }*/

}
