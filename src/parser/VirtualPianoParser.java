package parser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import music.*;

public class VirtualPianoParser {
    private Map<Note, Integer> noteToMidiNumber = new HashMap<>();
    private Map<Note, Integer> noteToKeyCode    = new HashMap<>();
    private Map<Character, Note> charToNote     = new HashMap<>();
    private Map<Note, Character> noteToChar     = new HashMap<>();

    // note to midi number
    private Map<String, Integer> noteStringToMidiNumber = new HashMap<>();

    public int getMidiNumber(String noteString) {
        return this.noteStringToMidiNumber.get(noteString);
    }

    // note to char
    private Map<String, Character> noteStringToChar     = new HashMap<>();
    public char getNoteChar(String noteString) {
        return this.noteStringToChar.get(noteString);
    }

    private Set<Character> isShiftSet = new HashSet<>();

    private char[] numberShifts = { '!', '@', '#', '$', '%', '^', '&', '*', '(', ')' };

    {
        for (char isShiftChar = 'A'; isShiftChar <= 'Z'; isShiftChar++) {
            this.isShiftSet.add(isShiftChar);
        }

        for (char isShiftChar : numberShifts) {
            this.isShiftSet.add(isShiftChar);
        }

    }

    {
        try {
            this.parseConfigFile("./input/map.csv");
        } catch (ParsingException ignored) {}
    }

    public boolean isShiftChar(char c) {
        return this.isShiftSet.contains(c);
    }

    public int getMidiNumber(Note note) {
        return this.noteToMidiNumber.get(note);
    }

    public int getKeyCode(Note note) {
        return this.noteToKeyCode.get(note);
    }

    public char getChar(Note note) {
        return this.noteToChar.get(note);
    }

    public Note getNote(char charKeyBinding) {
        return this.charToNote.get(charKeyBinding);
    }

    public void parseConfigFile(String configFilePath) throws ParsingException {
        try {
            Pattern configFileLinePattern
                    = Pattern.compile("^(.),([" + Pitch.toConcatenatedString()
                    + "]#?[" + Octave.toConcatenatedString()
                    + "]),([0-9]+)$");

            Pattern notePattern = Pattern.compile(
                    "^([" + Pitch.toConcatenatedString() + "])"
                    + "(#?)"
                    + "([" + Octave.toConcatenatedString() + "])$"
            );

            BufferedReader bufferedReader = new BufferedReader(new FileReader(configFilePath));
            Stream<String> configFileLineStream = bufferedReader.lines();
            configFileLineStream
                    .filter(configFileLine -> configFileLinePattern.matcher(configFileLine).matches())
                    .forEach(configFileLine -> {
                        Matcher configLineMatcher = configFileLinePattern.matcher(configFileLine);
                        if (configLineMatcher.find()) {
                            Integer keyCode = configLineMatcher.group(1).codePointAt(0);
                            Character keyChar = configLineMatcher.group(1).charAt(0);
                            String noteString = configLineMatcher.group(2);
                            Integer midiNumber = Integer.parseInt(configLineMatcher.group(3));
                            Matcher noteMatcher = notePattern.matcher(noteString);
                            if (noteMatcher.find()) {
                                Note note =
                                        new Note(Duration.oneQuarter,
                                                Pitch.stringToPitch(noteMatcher.group(1)),
                                                !noteMatcher.group(2).isEmpty(),
                                                Octave.stringToOctave(noteMatcher.group(3)));

                                this.noteToKeyCode.put(note, keyCode);
                                this.noteToMidiNumber.put(note, midiNumber);
                                this.charToNote.put(keyChar, note);
                                this.noteToChar.put(note, keyChar);

                                // note string to midi number
                                this.noteStringToMidiNumber.put(noteString, midiNumber);
                                // note string to char
                                this.noteStringToChar.put(noteString, keyChar);
                            }

                        }


                    });

            bufferedReader.close();
        }
        catch (FileNotFoundException e) {
            throw new ParsingException("Configuration file not found");
        }
        catch (IOException e) {
            throw new ParsingException("IO exception occurred");
        }
    }

    public Composition parseComposition(String compositionFilePath) throws ParsingException {
        Composition composition = new Composition();
        try {
            //noinspection RegExpRedundantEscape
            Pattern chordPattern = Pattern.compile("^\\[([^\\]]+)\\](.*)");
            BufferedReader bufferedReader = new BufferedReader(new FileReader(compositionFilePath));
            Stream<String> compositionFileLineStream = bufferedReader.lines();
            compositionFileLineStream.forEach(compositionFileLine -> {
                while (!compositionFileLine.isEmpty()) {
                    Matcher chordMatcher = chordPattern.matcher(compositionFileLine);
                    if (chordMatcher.matches()) {
                        // is with []
                        if (chordMatcher.group(1).contains(" ")) {
                            // is short note sequence
                            for (int i = 0; i < chordMatcher.group(1).length(); i += 2) {
                                Note note = this.charToNote.get(chordMatcher.group(1).charAt(i)).clone();
                                note.setDuration(Duration.oneEight);
                                composition.add(note);
                            }

                        }
                        else {
                            // is chord
                            Chord chord = new Chord(Duration.oneQuarter);
                            for (int i = 0; i < chordMatcher.group(1).length(); i++) {
                                chord.add(this.charToNote.get(chordMatcher.group(1).charAt(i)));
                            }

                            composition.add(chord);
                        }

                        compositionFileLine = compositionFileLine.substring(1 + compositionFileLine.indexOf("]"));
                    }
                    else {
                        // is without []
                        switch (compositionFileLine.charAt(0)) {
                            case '|':
                                // is long pause
                                composition.add(new Pause(Duration.oneQuarter));
                                break;
                            case ' ':
                                // is short pause
                                composition.add(new Pause(Duration.oneEight));
                                break;
                            default:
                                // is long note
                                Note note = this.charToNote.get(compositionFileLine.charAt(0)).clone();
                                note.setDuration(Duration.oneQuarter);
                                composition.add(note);
                                break;
                        }

                        compositionFileLine = compositionFileLine.substring(1);
                    }


                }

            });

            bufferedReader.close();
        }
        catch (FileNotFoundException e) {
            throw new ParsingException("Composition file not found");
        }
        catch (IOException e) {
            throw new ParsingException("IO exception occurred");
        }

        return composition;
    }

    public static void main(String ... args) {
        try {
            VirtualPianoParser vpp = new VirtualPianoParser();
            vpp.parseConfigFile("C:\\Users\\Bogdan\\Downloads\\map.csv");
            PianoPlayer player = new PianoPlayer();
            player.add(vpp.parseComposition("C:\\Users\\Bogdan\\Documents\\GitHub\\VirtualPianoConverter\\VirtualPianoConverter\\resource\\input\\ode_to_joy.txt"));
            //player.start();
        }
        catch (ParsingException ignored) {}

    }

}
