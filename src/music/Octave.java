package music;

public enum Octave {
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6);

    private int value;

    public static Octave stringToOctave(String string) {
        if (string.matches("[" + Octave.toConcatenatedString() + "]")) {
            switch (Integer.parseInt(string)) {
                case 2:
                    return TWO;
                case 3:
                    return THREE;
                case 4:
                    return FOUR;
                case 5:
                    return FIVE;
                case 6:
                    return SIX;
            }
        }

        return null;
    }

    Octave(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public static String toConcatenatedString() {
        StringBuilder sb = new StringBuilder();
        for (Octave octave : Octave.values()) {
            sb.append(octave.getValue());
        }

        return sb.toString();
    }

}
