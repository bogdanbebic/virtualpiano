package music;

import graphics.CompositionVisualizer;
import parser.VirtualPianoParser;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Chord extends MusicSymbol {

    private List<Note> notesInChord = new ArrayList<>();

    public void add(Note note) {
        note.setDuration(super.getDuration());
        this.notesInChord.add(note);
    }

    public Chord(Duration duration) {
        super(duration);
    }

    public List<MidiEvent> toMidiStart(VirtualPianoParser parser, long currentMidiTick) throws InvalidMidiDataException {
        List<MidiEvent> ret = new ArrayList<>();
        for (Note note : this.notesInChord) {
            ret.add(note.toMidiStart(parser, currentMidiTick));
        }

        return ret;
    }

    public List<MidiEvent> toMidiEnd(VirtualPianoParser parser, long currentMidiTick) throws InvalidMidiDataException {
        List<MidiEvent> ret = new ArrayList<>();
        for (Note note : this.notesInChord) {
            ret.add(note.toMidiEnd(parser, currentMidiTick));
        }

        return ret;
    }

    public String toTxt(VirtualPianoParser parser) {
        StringBuilder stringBuilder = new StringBuilder();
        notesInChord.forEach(note -> stringBuilder.append(note.toTxt(parser)));
        return "[" + stringBuilder.toString() + "]";
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        notesInChord.forEach(note -> stringBuilder.append(note.toString()));
        return "[" + stringBuilder.toString() + "]";
    }

    @Override
    public void draw(Graphics graphics, int startX, int startY, int width, boolean showNoteBindings, VirtualPianoParser parser) {
        int currentY = startY;
        for (Note note : this.notesInChord) {
            note.draw(graphics, startX, currentY, width, showNoteBindings, parser);
            currentY += CompositionVisualizer.MUSIC_SYMBOL_HEIGHT;
        }

    }

    @Override
    public void startPlaying(VirtualPianoParser parser) {
        this.notesInChord.forEach(note -> note.startPlaying(parser));
    }

    @Override
    public void stopPlaying(VirtualPianoParser parser) {
        this.notesInChord.forEach(note -> note.stopPlaying(parser));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Chord chord = (Chord) obj;
        chord.notesInChord.forEach(this.notesInChord::remove);
        return this.notesInChord.isEmpty() || obj.equals(this);
    }

    public static void main(String ... args) {
        Chord c1 = new Chord(Duration.oneQuarter),
                c2 = new Chord(Duration.oneQuarter);

        c1.add(new Note(Duration.oneQuarter, Pitch.C, false, Octave.THREE));
        c1.add(new Note(Duration.oneQuarter, Pitch.D, false, Octave.THREE));
        c1.add(new Note(Duration.oneQuarter, Pitch.F, false, Octave.THREE));


        c2.add(new Note(Duration.oneQuarter, Pitch.D, false, Octave.THREE));
        c2.add(new Note(Duration.oneQuarter, Pitch.C, false, Octave.THREE));
        c2.add(new Note(Duration.oneQuarter, Pitch.F, false, Octave.THREE));

        System.out.println(c1.equals(c2));
        System.out.println(c2.equals(c1));
    }

}
