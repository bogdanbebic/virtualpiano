package music;

import parser.VirtualPianoParser;

import java.awt.*;

abstract public class MusicSymbol implements PianoPlayable {
    private Duration duration;

    public MusicSymbol(Duration duration) {
        this.duration = duration;
    }

    public Duration getDuration() {
        return this.duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public abstract void draw(Graphics graphics, int startX, int startY, int width, boolean showNoteBindings, VirtualPianoParser parser);

    abstract public void startPlaying(VirtualPianoParser parser);
    abstract public void stopPlaying(VirtualPianoParser parser);

    @Override
    public void play(VirtualPianoParser parser) {
        this.startPlaying(parser);
        try {
            Thread.sleep(this.duration.toMilis());
        } catch (InterruptedException ignored) {}

        this.stopPlaying(parser);
    }

    @Override
    abstract public String toString();

}
