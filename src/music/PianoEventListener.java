package music;

public interface PianoEventListener {
    void dispatchEvent(Note note);
}
