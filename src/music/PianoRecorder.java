package music;

import java.util.ArrayList;
import java.util.List;

public class PianoRecorder implements PianoEventListener {
    private Composition composition = new Composition();
    private boolean isRecording = false;

    private List<Note> list = new ArrayList<>();
    private long firstNoteTimeMillis = 0;
    private long lastNoteMillis = 0;

    public void startRecording() {
        this.composition = new Composition();
        this.isRecording = true;
    }

    private void stopRecording() {
        this.isRecording = false;
    }

    public Composition getRecordedComposition() {
        this.stopRecording();
        Chord chord = new Chord(Duration.oneQuarter);
        this.list.forEach(chord::add);
        this.composition.add(chord);
        return this.composition;
    }

    @Override
    public void dispatchEvent(Note note) {
        if (!this.isRecording) {
            return;
        }

        long currentTimeMillis = System.currentTimeMillis();
        if (this.lastNoteMillis != 0) {
            long numberOfPauses = (currentTimeMillis - this.lastNoteMillis) / Duration.oneEight.toMilis();
            for (long i = 0; i < numberOfPauses; i++) {
                this.composition.add(new Pause(Duration.oneQuarter));
            }

        }

        this.lastNoteMillis = currentTimeMillis;

        if (note.getDuration() == Duration.oneEight) {
            Chord chord = new Chord(Duration.oneQuarter);
            this.list.forEach(chord::add);
            this.composition.add(chord);
            this.list.clear();
            this.firstNoteTimeMillis = 0;
            this.composition.add(note);
        }
        else {
            synchronized (this) {
                if (this.list.isEmpty()) {
                    this.firstNoteTimeMillis = currentTimeMillis;
                }
                else if (currentTimeMillis - this.firstNoteTimeMillis < Duration.oneEight.toMilis() / 2) {
                    if (this.list.size() == 1) {
                        this.list.forEach(this.composition::add);
                        this.list.clear();
                    }
                    else {
                        Chord chord = new Chord(Duration.oneQuarter);
                        this.list.forEach(chord::add);
                        this.list.clear();
                        this.composition.add(chord);
                        this.firstNoteTimeMillis = currentTimeMillis;
                    }

                }

                this.list.add(note);
            }

        }

    }

}
