package music;

import graphics.CompositionVisualizer;
import parser.VirtualPianoParser;

import java.awt.*;

public class Pause extends MusicSymbol {

    public Pause(Duration duration) {
        super(duration);
    }

    @Override
    public void draw(Graphics graphics, int startX, int startY, int width, boolean showNoteBindings, VirtualPianoParser parser) {
        if (this.getDuration() == Duration.oneQuarter) {
            graphics.setColor(Color.red);
        }
        else if (this.getDuration() == Duration.oneEight) {
            graphics.setColor(Color.green);
        }

        graphics.fillRect(startX, startY, width, CompositionVisualizer.MUSIC_SYMBOL_HEIGHT);
    }

    @Override
    public void startPlaying(VirtualPianoParser parser) {}

    @Override
    public void stopPlaying(VirtualPianoParser parser) {}

    @Override
    public String toString() {
        if (this.getDuration() == Duration.oneEight) {
            return " ";
        }
        else if (this.getDuration() == Duration.oneQuarter) {
            return "|";
        }

        return null;
    }

}
