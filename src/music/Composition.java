package music;

import export.MidiFormatter;
import graphics.VirtualPiano;
import parser.VirtualPianoParser;

import javax.sound.midi.*;
import java.util.ArrayList;
import java.util.List;

public class Composition implements PianoPlayable {
    private List<PianoPlayable> pianoPlayableList = new ArrayList<>();

    public String toTxt(VirtualPianoParser parser) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean isEightNoteSequenceStarted = false;
        for (PianoPlayable pianoPlayable : this.pianoPlayableList) {
            if (pianoPlayable instanceof Pause) {
                if (isEightNoteSequenceStarted) {
                    stringBuilder.append("]");
                    isEightNoteSequenceStarted = false;
                }

                String ret = pianoPlayable.toString();
                if (ret != null) {
                    stringBuilder.append(ret);
                }

            }
            else if (pianoPlayable instanceof Chord) {
                if (isEightNoteSequenceStarted) {
                    stringBuilder.append("]");
                    isEightNoteSequenceStarted = false;
                }

                stringBuilder.append(((Chord) pianoPlayable).toTxt(parser));
            }
            else if (pianoPlayable instanceof Note) {
                Note note = (Note) pianoPlayable;
                if (isEightNoteSequenceStarted && note.getDuration() == Duration.oneQuarter) {
                    stringBuilder.append("]");
                    isEightNoteSequenceStarted = false;
                }

                if (!isEightNoteSequenceStarted && note.getDuration() == Duration.oneEight) {
                    stringBuilder.append("[");
                    isEightNoteSequenceStarted = true;
                } else if (isEightNoteSequenceStarted && note.getDuration() == Duration.oneEight) {
                    stringBuilder.append(" ");
                }

                stringBuilder.append(note.toTxt(parser));
            }

        }

        if (isEightNoteSequenceStarted) {
            stringBuilder.append("]");
        }

        return stringBuilder.toString();
    }


    // Used in CompositionVisualizer
    public PianoPlayable get(int index) {
        return this.pianoPlayableList.get(index);
    }

    // Used in CompositionVisualizer
    public int size() {
        return this.pianoPlayableList.size();
    }

    // toMidi variable
    private long currentMidiTick = 0;

    public Sequence toMidi(VirtualPianoParser parser) throws InvalidMidiDataException {
        final int SPEED = 48 * 3;
        Sequence sequence = new Sequence(
                Sequence.PPQ,
                SPEED
        );

        Track track = sequence.createTrack();

        // General MIDI sysex -- turn on General MIDI sound set ****
        byte[] bytes = { (byte) 0xF0, 0x7E, 0x7F, 0x09, 0x01, (byte) 0xF7 };
        SysexMessage sysexMessage = new SysexMessage(bytes, bytes.length);
        MidiEvent midiEvent = new MidiEvent(sysexMessage, 0);
        track.add(midiEvent);

        // set tempo
        byte[] bytesTempo = { 0x2, 0x00, 0x00 };
        MetaMessage metaMessage = new MetaMessage(0x51, bytesTempo, bytesTempo.length);
        midiEvent = new MidiEvent(metaMessage, 0);
        track.add(midiEvent);

        // set track name
        String trackName = "track1";
        metaMessage = new MetaMessage(0x03, trackName.getBytes(), trackName.length());
        midiEvent = new MidiEvent(metaMessage, 0);
        track.add(midiEvent);

        // set omni on
        ShortMessage shortMessage = new ShortMessage(0xB0, 0x7D, 0x00);
        midiEvent = new MidiEvent(shortMessage, 0);
        track.add(midiEvent);

        // set poly on
        shortMessage = new ShortMessage(0xB0, 0x7F, 0x00);
        midiEvent = new MidiEvent(shortMessage, 0);
        track.add(midiEvent);

        // set instrument to piano
        shortMessage = new ShortMessage(0xC0, 0x00, 0x00);
        midiEvent = new MidiEvent(shortMessage, 0);
        track.add(midiEvent);

        Composition.this.currentMidiTick = 0;
        this.pianoPlayableList.forEach(pianoPlayable -> {
            try {
                if (pianoPlayable instanceof MusicSymbol) {
                    MusicSymbol current = (MusicSymbol) pianoPlayable;

                    // start midi event
                    if (current instanceof Note) {
                        track.add(((Note) current).toMidiStart(parser, Composition.this.currentMidiTick));
                    }
                    else if (current instanceof Chord) {
                        ((Chord) current).toMidiStart(parser, currentMidiTick).forEach(track::add);
                    }

                    if (current.getDuration() == Duration.oneQuarter) {
                        Composition.this.currentMidiTick += MidiFormatter.QUARTER_NOTE_MIDI_DURATION;
                    } else if (current.getDuration() == Duration.oneEight) {
                        Composition.this.currentMidiTick += MidiFormatter.EIGHT_NOTE_MIDI_DURATION;
                    }

                    // end midi event
                    if (current instanceof Note) {
                        track.add(((Note) current).toMidiEnd(parser, Composition.this.currentMidiTick));
                    }
                    else if (current instanceof Chord) {
                        ((Chord) current).toMidiEnd(parser, currentMidiTick).forEach(track::add);
                    }

                }

            }
            catch (InvalidMidiDataException ignored) {}

        });

        // set end of track
        byte[] emptyArray = {};
        metaMessage = new MetaMessage(0x2F, emptyArray, emptyArray.length);
        midiEvent = new MidiEvent(metaMessage, Composition.this.currentMidiTick);
        track.add(midiEvent);

        return sequence;
    }

    public void add(PianoPlayable pianoPlayable) {
        this.pianoPlayableList.add(pianoPlayable);
    }

    @Override
    public void play(VirtualPianoParser parser) {
        for (PianoPlayable pianoPlayable : this.pianoPlayableList) {
            pianoPlayable.play(parser);
        }

    }

}
