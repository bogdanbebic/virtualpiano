package music;

public enum Pitch {
    C, D, E, F, G, A, B;

    public static Pitch stringToPitch(String string) {
        if (string.matches("[" + Pitch.toConcatenatedString() + "]")) {
            switch (string) {
                case "C":
                    return C;
                case "D":
                    return D;
                case "E":
                    return E;
                case "F":
                    return F;
                case "G":
                    return G;
                case "A":
                    return A;
                case "B":
                    return B;
            }

        }

        return null;
    }

    public static String toConcatenatedString() {
        StringBuilder sb = new StringBuilder();
        for (Pitch pitch : Pitch.values()) {
            sb.append(pitch);
        }

        return sb.toString();
    }

}
