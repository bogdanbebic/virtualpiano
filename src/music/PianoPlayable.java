package music;

import parser.VirtualPianoParser;

@FunctionalInterface
public interface PianoPlayable {
    void play(VirtualPianoParser parser);
}
