package music;

import parser.VirtualPianoParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class PianoPlayer extends Thread {
    private List<PianoPlayable> playlist = new ArrayList<>();
    private VirtualPianoParser parser;

    public void add(PianoPlayable pianoPlayable) {
        this.playlist.add(pianoPlayable);
    }

    public PianoPlayer() {}

    public PianoPlayer(List<PianoPlayable> playlist) {
        this.playlist = playlist;
    }

    public PianoPlayer(PianoPlayable ... playlist) {
        Collections.addAll(this.playlist, playlist);
    }

    @Override
    public void run() {
        super.run();
        for (PianoPlayable pianoPlayable : this.playlist) {
            pianoPlayable.play(this.parser);
        }

    }

    public void setParser(VirtualPianoParser parser) {
        this.parser = parser;
    }

}
