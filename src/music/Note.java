package music;

import graphics.CompositionVisualizer;
import parser.VirtualPianoParser;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.ShortMessage;
import java.awt.*;
import java.awt.event.KeyEvent;

public class Note extends MusicSymbol implements Cloneable {
    private Pitch pitch;
    private boolean isSharp = false;
    private Octave octave;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note note = (Note) o;
        return isSharp == note.isSharp &&
                pitch == note.pitch &&
                octave == note.octave;
    }

    public Note(Duration duration, Pitch pitch, Octave octave) {
        super(duration);
        this.pitch = pitch;
        this.octave = octave;
    }

    public Note(Duration duration, Pitch pitch, boolean isSharp, Octave octave) {
        this(duration, pitch, octave);
        this.isSharp = isSharp;
    }

    private int getKeyCode(VirtualPianoParser parser) {
        char keyCodeRepresentation = Character.toLowerCase(parser.getNoteChar(this.toString().toUpperCase()));
        if (Character.isLetter(keyCodeRepresentation)) {
            return KeyEvent.VK_A + keyCodeRepresentation - 'a';
        }
        else if (Character.isDigit(this.toString().charAt(0))) {
            return KeyEvent.VK_1 + keyCodeRepresentation - '1';
        }
        else {
            String digitUpper = ")!@#$%^&*(";
            int digitUpperIndex = digitUpper.indexOf(keyCodeRepresentation);
            if (digitUpperIndex == -1) {
                return KeyEvent.VK_M;   // Exception
            }

            return KeyEvent.VK_0 + digitUpperIndex;
        }

    }

    @Override
    public void draw(Graphics graphics, int startX, int startY, int width, boolean showNoteBindings, VirtualPianoParser parser) {
        if (this.getDuration() == Duration.oneQuarter) {
            graphics.setColor(Color.blue);
        }
        else if (this.getDuration() == Duration.oneEight) {
            graphics.setColor(Color.cyan);
        }

        graphics.fillRect(startX, startY, width, CompositionVisualizer.MUSIC_SYMBOL_HEIGHT);

        // set label
        graphics.setColor(Color.black);
        int fontSize = (int) (0.8 * CompositionVisualizer.MUSIC_SYMBOL_HEIGHT);
        Font font = new Font("Arial", Font.BOLD, fontSize);
        FontMetrics metrics = graphics.getFontMetrics(font);
        graphics.setFont(font);
        String label = showNoteBindings ? Character.toString(this.toTxt(parser)) : this.toString();
        graphics.drawString(label, startX + width / 2 - metrics.stringWidth(label) / 2,
                startY + ((CompositionVisualizer.MUSIC_SYMBOL_HEIGHT - metrics.getHeight()) / 2)
                        + metrics.getAscent());

    }

    @Override
    public void startPlaying(VirtualPianoParser parser) {
        int keyCode = this.getKeyCode(parser);
        try {
            Robot robot = new Robot();
            if (this.isSharp) {
                robot.keyPress(KeyEvent.VK_SHIFT);
            }

            robot.keyPress(keyCode);
        }
        catch (AWTException ignored) {}
    }

    @Override
    public void stopPlaying(VirtualPianoParser parser) {
        int keyCode = this.getKeyCode(parser);
        try {
            Robot robot = new Robot();
            robot.keyRelease(keyCode);
            if (this.isSharp) {
                robot.keyRelease(KeyEvent.VK_SHIFT);
            }

        }
        catch (AWTException ignored) {}
    }

    public MidiEvent toMidiStart(VirtualPianoParser parser, long currentMidiTick) throws InvalidMidiDataException {
        Note note = this.clone();
        note.setDuration(Duration.oneQuarter);
        ShortMessage startMessage = new ShortMessage(
                0x90, parser.getMidiNumber(this.toString().toUpperCase()), 0x60
        );

        return new MidiEvent(startMessage, currentMidiTick);
    }

    public MidiEvent toMidiEnd(VirtualPianoParser parser, long currentMidiTick) throws InvalidMidiDataException {
        Note note = this.clone();
        note.setDuration(Duration.oneQuarter);
        ShortMessage endMessage = new ShortMessage(
                0x80, parser.getMidiNumber(this.toString().toUpperCase()), 0x40
        );

        return new MidiEvent(endMessage, currentMidiTick);
    }

    public char toTxt(VirtualPianoParser parser) {
        return parser.getNoteChar(this.toString().toUpperCase());
    }

    @Override
    public String toString() {
        String ret = this.pitch.toString();
        if (this.getDuration() == Duration.oneEight) {
            ret = ret.toLowerCase();
        }

        if (this.isSharp) {
            ret += "#";
        }

        return ret + octave.getValue();
    }

    public static void main(String ... args) {
        Note note1 = new Note(Duration.oneEight, Pitch.C, Octave.THREE);
        System.out.println(note1);
        Note note2 = new Note(Duration.oneQuarter, Pitch.E, Octave.TWO);
        System.out.println(note2);
    }

    @Override
    public Note clone() {
        try {
            super.clone();
        } catch (CloneNotSupportedException ignored) {}

        return new Note(this.getDuration(), this.pitch, this.isSharp, this.octave);
    }
}
