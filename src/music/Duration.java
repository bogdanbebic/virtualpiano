package music;


import com.sun.istack.internal.NotNull;

/**
 * Duration of music - represented as a fraction
 */
public class Duration {
    private int numerator, denominator;

    private static int ONE_QUARTER_MILLIS = 400, ONE_EIGHT_MILLIS = 200;

    public static Duration oneQuarter = new Duration(1, 4);
    public static Duration oneEight = new Duration(1, 8);

    public Duration(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        int greatest_common_divisor = Duration.gcd(numerator, denominator);
        if (greatest_common_divisor != 1) {
            this.numerator /= greatest_common_divisor;
            this.denominator /= greatest_common_divisor;
        }

    }

    public int toMilis() {
        if (this.equals(Duration.oneEight)) {
            return Duration.ONE_EIGHT_MILLIS;
        }

        return Duration.ONE_QUARTER_MILLIS;
    }

    public int getNumerator() {
        return this.numerator;
    }

    public int getDenominator() {
        return this.denominator;
    }

    public static Duration absDifference(@NotNull Duration d1, @NotNull Duration d2) {
        int den = lcm(d1.denominator, d2.denominator);
        int greatest_common_divisor = Duration.gcd(d1.denominator, d2.denominator);
        int num1 = d1.numerator * (d2.denominator / greatest_common_divisor);
        int num2 = d2.numerator * (d1.denominator / greatest_common_divisor);
        int num = num1 >= num2 ? num1 - num2 : num2 - num1;
        return new Duration(num, den);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || this.getClass() != other.getClass()) return false;
        Duration duration = (Duration) other;
        return this.numerator == duration.numerator &&
                this.denominator == duration.denominator;
    }

    @Override
    public String toString() {
        return this.numerator + "/" + this.denominator;
    }

    private static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }

        return gcd(b, a % b);
    }

    private static int lcm(int a, int b) {
        // division first, to avoid overflow
        return a * (b / gcd(a, b));
    }

    public static void main(String ... args) {
        Duration d1 = new Duration(4, 4),
                 d2 = new Duration(3, 4);

        Duration res = absDifference(d1, d2);
        System.out.println(res);
    }

}
