package export;

public class ExportException extends Exception {
    public ExportException(String message) {
        super(message);
    }

}
