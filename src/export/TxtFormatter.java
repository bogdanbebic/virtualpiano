package export;

import music.Composition;
import parser.VirtualPianoParser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TxtFormatter extends Formatter {
    public TxtFormatter(VirtualPianoParser parser) {
        super(parser);
    }

    @Override
    public void export(String filepath, Composition composition) throws ExportException {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filepath));
            bufferedWriter.write(composition.toTxt(super.parser));
            bufferedWriter.flush();
            bufferedWriter.close();
        }
        catch (IOException e) {
            throw new ExportException(e.getMessage());
        }

    }

}
