package export;

import music.Composition;
import parser.VirtualPianoParser;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import java.io.File;
import java.io.IOException;

public class MidiFormatter extends Formatter {
    public static final int QUARTER_NOTE_MIDI_DURATION = 1000 / 4;
    public static final int EIGHT_NOTE_MIDI_DURATION = QUARTER_NOTE_MIDI_DURATION / 2;

    public MidiFormatter(VirtualPianoParser parser) {
        super(parser);
    }

    @Override
    public void export(String filepath, Composition composition) throws ExportException {
        try {
            MidiSystem.write(composition.toMidi(super.parser), 1, new File(filepath));
        }
        catch (IOException | InvalidMidiDataException e) {
            throw new ExportException(e.getMessage());
        }
    }

}
