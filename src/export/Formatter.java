package export;

import music.Composition;
import parser.VirtualPianoParser;

public abstract class Formatter {
    protected VirtualPianoParser parser;

    public abstract void export(String filepath, Composition composition) throws ExportException;

    protected Formatter(VirtualPianoParser parser) {
        this.parser = parser;
    }

}
